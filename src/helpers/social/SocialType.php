<?php

namespace blakit\helpers\social;

use MyCLabs\Enum\Enum;

class SocialType extends Enum
{
    const VKONTAKTE = 'VKONTAKTE';
    const FACEBOOK = 'FACEBOOK';
    const GOOGLE = 'GOOGLE';
    const INSTAGRAM = 'INSTAGRAM';
    const TWITTER = 'TWITTER';
}