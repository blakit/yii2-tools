<?php

namespace blakit\helpers\social;

use blakit\helpers\social\authenticators\FacebookAuthenticator;
use blakit\helpers\social\authenticators\GoogleAuthenticator;
use blakit\helpers\social\authenticators\VkontakteAuthenticator;
use blakit\helpers\social\authenticators\TwitterAuthenticator;
use blakit\helpers\social\authenticators\InstagramAuthenticator;


class SocialAuthenticator
{
    /**
     * @param SocialType $socialType
     * @param string $token
     *
     * @return SocialUser|null
     */
    public static function getUserByAccessToken(SocialType $socialType, $token)
    {
        switch ($socialType) {

            case SocialType::VKONTAKTE:
                $authenticator = new VkontakteAuthenticator();
                break;

            case SocialType::FACEBOOK:
                $authenticator = new FacebookAuthenticator();
                break;

            case SocialType::GOOGLE:
                $authenticator = new GoogleAuthenticator();
                break;

            case SocialType::TWITTER:
                $authenticator = new TwitterAuthenticator();
                break;

            case SocialType::INSTAGRAM:
                $authenticator = new InstagramAuthenticator();
                break;

            default:
                return null;
        }

        $user = $authenticator->getUserByAccessToken($token);
        if (!$user) {
            return null;
        }

        $user->setSocialType($socialType);

        return $user;
    }
}