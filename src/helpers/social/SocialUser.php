<?php

namespace blakit\helpers\social;

use blakit\constants\Gender;

class SocialUser
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $first_name;

    /** @var $string */
    private $last_name;

    /** @var string */
    private $email;

    /** @var string */
    private $photo;

    /** @var Gender */
    private $gender;

    /** @var \DateTime */
    private $birthday;

    /** @var SocialType */
    private $social_type;

    public function setSocialId($id)
    {
        $this->id = $id;
    }

    public function getSocialId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setFirstName($value)
    {
        $this->first_name = $value;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setLastName($value)
    {
        $this->last_name = $value;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function getBirthDay()
    {
        return $this->birthday;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setSocialType($type)
    {
        $this->social_type = $type;
    }

    public function getSocialType()
    {
        return $this->social_type;
    }
}
