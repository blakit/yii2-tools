<?php

namespace blakit\helpers\social\authenticators;

use blakit\constants\Gender;
use blakit\helpers\social\SocialUser;

class InstagramAuthenticator implements IAuthenticator
{
    public function getUserByAccessToken($token)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.instagram.com/v1/users/self/?access_token=" . $token,
            CURLOPT_SSL_VERIFYPEER => true
        ));

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($info['http_code'] != 200) {
            return null;
        }

        $response = json_decode($response, true);

        $response = $response['data'];
        
        $user = new SocialUser();

        $full_name = explode(' ', $response['full_name']);

        $user->setSocialId($response['id']);

        if (isset($response['username'])) {
            $user->setName($response['username']);
        }

        if (isset($full_name[0])) {
            $user->setFirstName($full_name[0]);
        }

        if (isset($full_name[1])) {
            $user->setLastName($full_name[1]);
        }

        if (isset($response['profile_picture'])) {
            $user->setPhoto($response['profile_picture']);
        }

        return $user;
    }
}