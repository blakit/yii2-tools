<?php

namespace blakit\helpers\social\authenticators;

use blakit\constants\Gender;
use blakit\helpers\social\SocialUser;

class VkontakteAuthenticator implements IAuthenticator
{
    public function getUserByAccessToken($token)
    {
        $user_details = 'https://api.vk.com/method/users.get?v=5.0&access_token=' . $token . '&fields=uid,first_name,email,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo,photo_max_orig';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $user_details,
            CURLOPT_SSL_VERIFYPEER => true
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response);

        $response = $response->response[0];

        $user = new SocialUser();

        $user->setGender($response->sex == 2 ? Gender::MALE : Gender::FEMALE);
        $user->setFirstName($response->first_name);
        $user->setLastName($response->last_name);
        $user->setName($response->last_name . ' ' . $response->first_name);
        $user->setSocialId($response->id);
        $user->setBirthday(\DateTime::createFromFormat('d.m.Y', $response->bdate));
        $user->setPhoto($response->photo_max_orig);

        return $user;
    }
}