<?php

namespace blakit\helpers\social\authenticators;

use blakit\constants\Gender;
use blakit\helpers\social\SocialUser;

class GoogleAuthenticator implements IAuthenticator
{
    public function getUserByIdToken($token)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" . urlencode($token),
            CURLOPT_SSL_VERIFYPEER => true
        ));

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($info['http_code'] != 200) {
            return null;
        }

        $response = json_decode($response, true);

        $user = new SocialUser();

        $user->setSocialId($response['sub']);

        if (isset($response['name'])) {
            $user->setName($response['name']);
        }

        if (isset($response['given_name'])) {
            $user->setFirstName($response['given_name']);
        }

        if (isset($response['family_name'])) {
            $user->setLastName($response['family_name']);
        }

        if (isset($response['gender'])) {
            $user->setGender($response['gender'] == 'male' ? Gender::MALE : Gender::FEMALE);
        }

        if (isset($response['email'])) {
            $user->setEmail($response['email']);
        }

        if (isset($response['picture'])) {
            $user->setPhoto($response['picture']);
        }

        return $user;
    }

    public function getUserByAccessToken($token)
    {
        if (strlen($token) > 255) {
            return $this->getUserByIdToken($token);
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://www.googleapis.com/oauth2/v2/userinfo",
            CURLOPT_SSL_VERIFYPEER => true
        ));

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));


        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($info['http_code'] != 200) {
            return null;
        }

        $response = json_decode($response, true);

        $user = new SocialUser();

        $user->setSocialId($response['id']);

        if (isset($response['name'])) {
            $user->setName($response['name']);
        }

        if (isset($response['given_name'])) {
            $user->setFirstName($response['given_name']);
        }

        if (isset($response['family_name'])) {
            $user->setLastName($response['family_name']);
        }

        if (isset($response['gender'])) {
            $user->setGender($response['gender'] == 'male' ? Gender::MALE : Gender::FEMALE);
        }

        if (isset($response['email'])) {
            $user->setEmail($response['email']);
        }

        if (isset($response['picture'])) {
            $user->setPhoto($response['picture']);
        }

        return $user;
    }
}