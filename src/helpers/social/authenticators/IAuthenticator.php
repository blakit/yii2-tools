<?php

namespace blakit\helpers\social\authenticators;

use blakit\helpers\social\SocialUser;

interface IAuthenticator
{
    /**
     * @param string $token;
     *
     * @return SocialUser|null
     */
    public function getUserByAccessToken($token);
}