<?php

namespace blakit\helpers\social\authenticators;

use blakit\constants\Gender;
use blakit\helpers\social\SocialUser;

class FacebookAuthenticator implements IAuthenticator
{
    public function getUserByAccessToken($token)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://graph.facebook.com/me?fields=id,email,name,first_name,last_name,picture.type(large),gender&access_token=" . $token,
            CURLOPT_SSL_VERIFYPEER => true
        ));

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($info['http_code'] != 200) {
            return null;
        }

        $response = json_decode($response, true);

        $user = new SocialUser();

        $user->setSocialId($response['id']);

        if (isset($response['name'])) {
            $user->setName($response['name']);
        }

        if (isset($response['first_name'])) {
            $user->setFirstName($response['first_name']);
        }

        if (isset($response['last_name'])) {
            $user->setLastName($response['last_name']);
        }

        if (isset($response['gender'])) {
            $user->setGender($response['gender'] == 'male' ? Gender::MALE : Gender::FEMALE);
        }

        if (isset($response['email'])) {
            $user->setEmail($response['email']);
        }

        if (isset($response['picture'])) {
            $user->setPhoto($response['picture']['data']['url']);
        }

        return $user;
    }
}