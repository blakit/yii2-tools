<?php

namespace blakit\helpers\sms;

interface ISmsProvider
{
    /**
     * @param string[]|string $phones
     * @param string $message
     * @return mixed
     */
    public function send($phones, $message);
}