<?php

namespace blakit\helpers\sms;

use yii\base\Component;

class SmscProvider extends Component implements ISmsProvider
{
    public $enabled = true;

    public $login;
    public $password;
    public $useSSL = false;
    public $options = [];

    const MSG_SMS = 0;
    const MSG_FLASH = 1;
    const MSG_WAP = 2;
    const MSG_HLR = 3;
    const MSG_BIN = 4;
    const MSG_HEX = 5;
    const MSG_PING = 6;
    const COST_NO = 0;
    const COST_ONLY = 1;
    const COST_TOTAL = 2;
    const COST_BALANCE = 3;
    const TRANSLIT_NONE = 0;
    const TRANSLIT_YES = 1;
    const TRANSLIT_ALT = 2;
    const FMT_PLAIN = 0;
    const FMT_PLAIN_ALT = 1;
    const FMT_XML = 2;
    const FMT_JSON = 3;
    const STATUS_PLAIN = 0;
    const STATUS_INFO = 1;
    const STATUS_INFO_EXT = 2;
    const CHARSET_UTF8 = 'utf-8';
    const CHARSET_KOI8 = 'koi8-r';
    const CHARSET_1251 = 'windows-1251';
    const ZONE_RU = 10;
    const ZONE_UA = 20;
    const ZONE_SNG = 30;
    const ZONE_1 = 1;
    const ZONE_2 = 2;
    const ZONE_3 = 3;

    private static $curl = null;

    /**
     * Инициализация.
     * Установка значений по умолчанию.
     */
    public function init()
    {
        $default = [
            'charset' => self::CHARSET_UTF8,
            'fmt' => self::FMT_JSON,
            'translit' => self::TRANSLIT_NONE,
            'type' => self::MSG_SMS,
            'cost' => self::COST_NO,
            'time' => null,
            'tz' => null,
            'period' => null,
            'freq' => null,
            'maxsms' => null,
            'err' => null
        ];
        $this->options = array_merge($default, $this->options);
        $this->password = md5($this->password);
    }

    /**
     * Проверка номеров на доступность в реальном времени.
     *
     * @param string|array $phones номера телефонов
     *
     * @return bool|string|\stdClass результат выполнения запроса в виде строки, объекта (FMT_JSON) или false в случае ошибки.
     */
    public function pingPhone($phones)
    {
        return $this->send($phones, null, null, ['type' => self::MSG_PING]);
    }


    /**
     * Удаляет из номера любые символы, кроме цифр.
     *
     * @param string $phone номер телефона
     *
     * @return string «чистый» номер телефона
     */
    public static function clearPhone($phone)
    {
        return preg_replace('~[^\d+]~', '', $phone);
    }

    /**
     * Самая умная функция.
     *
     * @access private
     *
     * @param string $resource
     * @param array $options
     *
     * @throws \InvalidArgumentException
     *
     * @return bool|string|\stdClass
     */
    private function sendRequest($resource, array $options)
    {
        $options = array_merge($this->options, $options);

        if (in_array($resource, ['status', 'info'])) {
            if (isset($options['phone']) && !empty($options['phone'])) {
                $options['phone'] = self::clearPhone($options['phone']);
            } else {
                throw new \InvalidArgumentException("The 'phone' parameter is empty.");
            }
        }
        $params = [
            'login=' . urlencode($this->login),
            'psw=' . urlencode($this->password),
        ];
        foreach ($options as $key => $value) {
            switch ($key) {
                case 'type':
                    if ($value > 0 && $value < count($this->_types)) {
                        $params[] = $this->_types[$value];
                    }
                    break;
                default:
                    if (!empty($value)) {
                        $params[] = $key . '=' . urlencode($value);
                    }
            }
        }
        $i = 0;
        do {
            (!$i) || sleep(2);
            $ret = $this->execRequest($resource, $params);
        } while ($ret == '' && ++$i < 3);
        if (($resource == 'info' || $resource == 'status') && $options['fmt'] == self::FMT_JSON) {
            if ($options['charset'] == self::CHARSET_1251) {
                $ret = mb_convert_encoding($ret, 'UTF-8', 'WINDOWS-1251');
            } elseif ($options['charset'] == self::CHARSET_KOI8) {
                $ret = mb_convert_encoding($ret, 'UTF-8', 'KOI8-R');
            }
        }
        return !empty($ret) ? $ret : false;
    }

    /**
     * Непосредственно выполнение запроса.
     *
     * @param string $resource
     * @param array $params
     *
     * @return string ответ сервера
     */
    private function execRequest($resource, array $params)
    {
        $url = ($this->useSSL ? 'https' : 'http') . '://smsc.ru/sys/' . $resource . '.php';
        $query = implode('&', $params);
        $isPOST = $resource === 'send' ? true : false;
        if (function_exists('curl_init')) {
            if (!self::$curl) {
                self::$curl = curl_init();
                curl_setopt_array(self::$curl, [
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_CONNECTTIMEOUT => 5,
                    CURLOPT_TIMEOUT => 10,
                ]);
            }
            if ($isPOST) {
                curl_setopt_array(self::$curl, [
                    CURLOPT_URL => $url,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $query,
                ]);
            } else {
                curl_setopt(self::$curl, CURLOPT_URL, $url . '?' . $query);
            }
            $response = curl_exec(self::$curl);
        } else {
            $options = ['timeout' => 5];
            if ($isPOST) {
                $options = array_merge($options, [
                    'method' => 'POST',
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'content' => $query,
                ]);
            } else {
                $url .= '?' . $query;
            }
            $response = file_get_contents($url, false, stream_context_create(['http' => $options]));
        }
        return $response;
    }


    /**
     * Отправка сообщения.
     *
     * @param string|array $phones номера телефонов
     * @param string $message текст сообщения
     *
     * @throws \InvalidArgumentException|SmsProviderException если список телефонов пуст или длина сообщения больше 800 символов
     * @return bool|string|\stdClass результат выполнения запроса в виде строки, объекта (FMT_JSON) или false в случае ошибки.
     */
    public function send($phones, $message)
    {
        if (!$this->enabled) {
            return true;
        }

        if (empty($phones)) {
            throw new \InvalidArgumentException("The 'phones' parameter is empty.");
        } else {
            if (is_array($phones)) {
                $phones = array_map(__CLASS__ . '::clearPhone', $phones);
                $phones = implode(';', $phones);
            } else {
                $phones = self::clearPhone($phones);
            }
        }

        if ($message !== null && empty($message)) {
            throw new \InvalidArgumentException('The message is empty.');
        } elseif (mb_strlen($message, 'UTF-8') > 800) {
            throw new \InvalidArgumentException('The maximum length of a message is 800 symbols.');
        }

        $options = [
            'phones' => $phones,
            'mes' => $message
        ];

        $response = $this->sendRequest('send', $options);

        try {
            $response = json_decode($response, true);
        } catch (\Exception $exception) {
            throw new SmsProviderException('Неправильный ответ');
        }

        if (isset($response['error'])) {
            throw new SmsProviderException($response['error_code'] . ' - ' . $response['error']);
        }

        return true;
    }
}