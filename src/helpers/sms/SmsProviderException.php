<?php

namespace blakit\helpers\sms;

use Throwable;

class SmsProviderException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Ошибка провайдера: '.$message, $code, $previous);
    }
}