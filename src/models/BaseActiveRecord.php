<?php

namespace blakit\models;

use blakit\behaviors\SoftDeleteBehavior;
use yii\db\ActiveRecord;

class BaseActiveRecord extends ActiveRecord
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        $model = new static;
        $behaviors = $model->behaviors();

        $table_name = static::tableName();

        $found = null;
        foreach ($behaviors as $behavior) {
            if ((isset($behavior['class']) && $behavior['class'] == SoftDeleteBehavior::className()) || ($behavior == SoftDeleteBehavior::className())) {
                /** @var SoftDeleteBehavior $behavior */
                $behavior = \Yii::createObject($behavior);

                $attributes = $behavior->softDeleteAttributeValues;

                $query = parent::find();

                $ind = 0;
                foreach ($attributes as $attribute => $value) {
                    $query->andWhere($table_name . '.' . $attribute . ' != :bar_deleted_' . $ind . ' OR '.$table_name.'.'.$attribute .' IS NULL', [
                        ':bar_deleted_' . $ind => $value
                    ]);
                }

                return $query;
            }
        }

        return parent::find();
    }

    public static function findWithDeleted()
    {
        return parent::find();
    }
}