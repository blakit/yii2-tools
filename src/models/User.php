<?php

namespace blakit\models;

use Firebase\JWT\JWT;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UnauthorizedHttpException;

/**
 * This is the model class for table "{{%users}}".
 *
 */
class User extends ActiveRecord implements IdentityInterface
{

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        try {
            $decoded = JWT::decode($token, \Yii::$app->params['jwt_key'], ['HS256']);
            if ($decoded->id) {
                return static::findIdentity($decoded->id);
            } else {
                return null;
            }
        } catch (\Exception $ex) {
            throw new UnauthorizedHttpException('JWT decode error');
        }
    }

    /**
     * @param string $role
     * @return ActiveQuery
     */
    public static function findByRole($role)
    {
        return self::find()->andWhere('role=:role', [':role' => $role]);
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        if ($password) {
            return \Yii::$app->security->validatePassword($password, $this->password_hash);
        } else {
            return false;
        }
    }

    public static function generateAuthKey()
    {
        return \Yii::$app->security->generateRandomString();
    }

    public function getPasswordHash($password)
    {
        return \Yii::$app->security->generatePasswordHash($password);
    }

    public function generateRandomPassword($length = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    /**
     * @return bool|string
     */
    public function setRandomPassword()
    {
        $password = $this->generateRandomPassword();
        $this->password_hash = $this->getPasswordHash($password);
        return $password;
    }
}
