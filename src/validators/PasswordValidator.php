<?php

namespace blakit\validators;

use yii\validators\Validator;

class PasswordValidator extends Validator
{
    public $message = '{attribute} must contain minimum 6 symbols';

    public function validateAttribute($model, $attribute)
    {
        $value = $model->{$attribute};

        if (strlen($value) < 6) {
            $this->addError($model, $attribute, $this->formatMessage($this->message, ['attribute' => $attribute]));
        }

        return true;
    }
}