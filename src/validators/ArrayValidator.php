<?php

namespace blakit\validators;

use yii\validators\Validator;

class ArrayValidator extends Validator
{
    private $model;
    private $attribute;

    public $elementValidator = null;
    public $elementValidatorParams = [];

    public $message = "{attribute} must be an array";

    public $attributeMessage = '{attribute}[{index}] - {error}';

    protected function validateElement($model, $index)
    {
        return true;
    }

    protected function addElementError($index, $error, $params = [])
    {
        if ($this->model instanceof \blakit\api\base\Model) {
            $this->model->addError($this->attribute, $error, $this->attribute . '.' . $index);
        } else {
            $this->addError($this->model, $this->attribute, $this->formatMessage($this->attributeMessage, [
                'attribute' => $this->attribute,
                'index' => $index,
                'error' => $error
            ]), $params);
        }
    }

    public function validateAttribute($model, $attribute)
    {
        $this->model = $model;
        $this->attribute = $attribute;

        $value = $model->$attribute;

        if (!is_array($value)) {
            $this->addError($model, $attribute, $this->formatMessage($this->message, ['attribute' => $attribute]));
            return false;
        }

        foreach ($value as $index => $item) {
            if ($this->elementValidator) {
                if (is_string($this->elementValidator)) {
                    if ($model->hasMethod($this->elementValidator)) {
                        $result = call_user_func_array([$model, $this->elementValidator], [$item, $index]);
                        if ($result !== true) {
                            $this->addElementError($index, $result);
                        }
                    } else {
                        $params = array_merge(['class' => $this->elementValidator], $this->elementValidatorParams);
                        /** @var Validator $validator */
                        $validator = \Yii::createObject($params);

                        $error = $validator->validateValue($item);
                        if ($error) {
                            $this->addElementError($index, $error[0], $error[1]);
                        }
                    }

                }
            } else {
                $this->validateElement($item, $index);
            }
        }

        return true;
    }
}