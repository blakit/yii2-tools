<?php

namespace blakit\validators;

use yii\validators\Validator;

class PhoneValidator extends Validator
{
    public $message = 'Invalid phone';

    public static function check($value)
    {
        if (empty($value)) {
            return true;
        }

        if ($value[0] != '+') {
            $value = '+' . $value;
        }

        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try {
            $phone = $phoneUtil->parse($value);
        } catch (NumberParseException $ex) {
            return false;
        }

        if ($phoneUtil->isValidNumber($phone) == false) {
            return false;
        }

        return true;
    }

    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;

        if ($value[0] != '+') {
            $value = '+' . $value;
        }

        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try {
            $phone = $phoneUtil->parse($value);
        } catch (NumberParseException $ex) {
            $model->addError($attribute, $this->message);
            return false;
        }

        if ($phoneUtil->isValidNumber($phone) == false) {
            $model->addError($attribute, $this->message);
            return false;
        }

        $model->$attribute = preg_replace('#[^\d]*#si', '', $value);

        return true;
    }
}