<?php

namespace blakit\constants;

class Gender extends \MyCLabs\Enum\Enum
{
    const MALE = 'MALE';
    const FEMALE = 'FEMALE';
}